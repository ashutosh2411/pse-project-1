# Tools and Validation Team
_Prepared by Adrian McDonald Tariang, 111501001 on 10-09-2018_

## Members
1. Adrian McDonald Tariang
2. Anish M M

## Objectives
* Create a CommandLine tool to execute Tests and Tools for the project
* Provide a Test Harness for Unit and Integration Testing in module development
* Develop Loggers to print to Console or File for the Tests
* Diagnostics feature for the module developers
* Collect Telemetry and Usage Statistics during program execution

## Dependencies 
All modules depend on Tools and Validation

The Test Harness is a stand-alone console application, while the Diagnostics and the Telemetry would need to be initialised or started by the first module running the MASTI software.

## Class Diagram
_Class Diagram prepared by Anish M M, 111501006 on 09-09-2018_

![Test Harness](test_harness_class_diagram.png)

## Activity Diagram
![Activity Diagram](path/to/the/activitydiagram.jpg)

## Interface 
The Exposed interfaces are :-

Logger
```
	public interface ILogger
    
	{
	    void LogInfo(string message);
	    void LogWarning(string message);
	    void LogError(string message);
        
	    // Send the logs to a prespecified mail id.
	    void MailLog();   
	}
        
```

Test
```
    public interface ITest
    {
        bool Run();   
    }
```

Telemetry
```
	public interface ITelemetry
    {
    	// Hold the data that needs to be collected by the module
    	IDictionary<string, object> dataCapture;

    	// Extract telemetry and statistics from new data
        void Calculate(object data);

        // Summarise captured data and prepare for telemetry storage
        void Summarise();
    }
```

## Internal Components

### CommandLine Tool
- Implement a Command line tool with functionality to aid development
- Handle Console flags to execute different functions
- This will allow the users to run their tests individually or all at once
- Provide status of number of success and failure
- Log the Test information along with their results to the Console or a file based on the developers choice
- Run tools such as 'Export Messages from Database to File'

### Test Harness
- Provide the developer with an interface to write their tests for Unit or Integration testing of their module components
- Also provide an Logger Interface for the developers to use in their tests for detail their execution
- Implement loggers to print to Console and File
- Allow the user to specify the verbosity of logging, ie, -v -vv -vvv
- Identify all the Tests available in the modules

### Diagnostics
- Provide the Developers with Logging Functionality within their module features
- This Will help track down the origin bugs and failures
- Feature to email Diagnostics log to development team on failure of program
- Diagnostic Log will be saved in Log[date][lognumber].txt
- Allow the user to specify the verbosity of logging, ie, -v -vv -vvv

### Telemetry
- Collect statistics of the user and operations performed on the tool such as number of  messages send and received, time spend on the application, etc
- The data can be collected for software improvements in later iterations
- Create an Interface for the developer to implement and specify the data to be collected

## Work Distribution
- Adrian McDonald Tariang
	- Test Harness
	- Commandline tool

- Anish M M
	- Diagnostics
	- Telemetry