
# Team Name
## Members
|||
|---------|---------------|
|Member 1 |Leader         |
|Member 2 |Designer       |
|Member 3 |Coder          |

## Objectives
* To provide an API for someone. 
* To Publish something to someone. 
* To subscribe for something from someone. 
## Dependencies 
The following components depend on my module. 
* **Module 1**: I provide this X using Y interface.
* **Module 2**: I provide this X using Y interface. 

My module depends on the following modules. 
* **Module 1**: For task X.
## Class Diagram
![Class Diagram](path/to/the/classdiagram.jpg)
## Activity Diagram
![Activity Diagram](path/to/the/activitydiagram.jpg)

## Interface 
Exposed and consumed interfaces.
```
This is the code for
	Interface to accomplish task 1.
	Make sure you follow proper casings. 
	camelCase for variables
	PascalCase for classes and Interfaces
	IMessaging and not Imessaging.
```
Consumed Interfaces:
```
Another interface.
```
## Internal Components
### Component 1
describe it
### Component 2
describe it
## Work Distribution
Who works on which component.