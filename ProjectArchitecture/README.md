# MASTI - Messenger Application for Student-Teacher Interaction

## Teams

1. Networking
2. Image Processing
3. Messaging
4. Schema
5. UI/UX
6. Testing & Validation
7. Persistence

## Functionalities of Modules

### Networking

1. Provide an API to send data to a specific IP and Port.
2. Provide an API where any module can subscribe to listen to specific events.
3. Every instance of the Networking object should create a new event handler.
4. Sending a Message
   - Use queues to store messages and try sending them accordingly (try multiple times).
   - If a message is sent dispatch an event accordingly (depending on who called it).
   - One way to achieve the above is to take an event as the second argument.
   - Dispatch the event with a structured message. (The respective team can then proceed by unpacking the message)
5. Recieveing a Message
   - Keep listening on a specific Port.
   - There can be only one application listening on a specific Port at any given time.
   - The Networking team should provide a singleton to work with.
   - Any module should be able to subscribe to that singleton with a specific tag.
   - Depending on the tag dispatch the specific event.
6. Every instantiation should return the same object (singleton).
7. Please use the conventions given for event naming (provided later).

### Image Processing

1. Subscribe to the object instantiated from Networking module.
2. You will need handle the events you want to listen to.
3. Provide an API where any module can subscribe to specific events.
4. You cannot directly link the subscribers to the Networking instance. Create your own Publisher.
5. Some possible events are `receivedScreen`, `receivedImage`, `errorSendScreen`, `errorScreenCapture`, `notificationStartedScreenCapture`, `notificationStoppedScreenCapture` etc.
6. Provide an API to start/stop screen capture.
7. Talk to the Schema team to decide the structure of the message.
8. Provide an API to retreive/store an image with specific parameters (date, time, sessionId etc.).

### Messaging

1. Subscribe to the object instantiated from Networking module.
2. You will need to handle the events you are interested in.
3. Provide an API where any module can subscribe to specific events.
4. You cannot directly link the subscribers to the Networking instance. Create your own Publisher.
5. Some possible events are `successText`, `failText`, `recievedText` etc.
6. Talk to the Schema team to decide the structure of the message.
7. Provide an API to retreive/store messages with specific parameters (date, time, sessionId etc.).
8. Give an API for deleting messages until a specific timestamp.

### Schema

1. Provide an API to encode and decode data for Image processing team and Text processing team (JSON preferably).
2. Make the message structure based on the requirements of the Text processing and Image processing team.

### UI/UX

1. You are free to design the display as you wish (I would suggest you to make the design in the end).
2. Instantiate objects from Text processing and Image processing.
3. You will need to subscribe and handle events dispached from the Text processing and Image processing publishers.
4. No Auth required. Keep it simple.
5. Use thread-safe data structures.
6. Need to create 2 modules, client and server.
7. Run a deletion mechanism on startup, provided by the Messaging team.
8. On start up, request a nick name and a option to store session or not.
9. If store, communicate it to the Messaging team and Image Processing team.

### Persistence

1. Provide API to store and receive data.
2. Your input will be a dictionary of key and values. Save accordingly.
3. All the data required should be in the dictionary provided.
4. You can request a few fields specifically.

### Tools and Validation

1. Provide an API to log data.
2. Provide an API to retrieve logs.
3. Create a Telemetry class to compute statictics.
4. Create a way to run all the test files in the module.

## Module Diagram

![Loading Module Diagram](moduleDiagram.png)

## Workflow
