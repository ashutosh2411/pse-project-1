# Team Name
This is the format for README. Please modify it and send a PR. 
## Members
||||
|---|------|---------------|
|Member 1 |Leader         ||
|Member 2 |Designer       ||
|Member 3 |Coder          ||

## Objectives
* Provide an API for X.
* Subscribe someone for something.
* Publish something for someone.
## Dependencies 
The following components depend on my module. 
* Module 1 
* Module 2 

My module depends on the following modules. 
* Module 1
## Class Diagram
![Class Diagram](path/to/the/image.jpg)

## Interface 
```
This is the code for
	Interface to accomplish task 1.
	Make sure you follow proper casings. 
	camelCase for variables
	PascalCase for classes and Interfaces
	IMessaging and not Imessaging.
```
## Work Distribution
who works on which component.